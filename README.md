## WPezClasses: Scripts Register

__Register your scripts in WordPress The ezWay.__

Because arrays are easier to configure, reuse, etc. They (i.e., the arrays) can also be filtered, and manipulated in ways that (hard) coding such things can't be.

Save yourself and your team from the peril of the WordPress status quo and stop hardcoding your scripts. Please. 

This class also integrates the WP function wp_add_inline_script(), to help consolidate / cut back on sprawl.   

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

    $arr_scripts = [
        'foo' => [
            'active' => true,
            'handle' => 'foo-js',
            'src' => 'https://cdn.xyz/foojs',
            'deps => [],
            'ver' => 'v1.2.3',
            'in_footer' => false,
            'hooks' => ['front', 'login'],
            'inline_data' => 'try{Typekit.load({ async: true });}catch(e){}'
            'inline_position' => 'after'
        
        ],
        'bar' => [
            ...
        ]
    ];
    
**'active'** - Simple bool on / off switch. Default: true; Optional (bool).

**'handle'** - Same as stock WP. *Required (string).

**'src'** - Same as stock WP. Default: false; Optional (string).

**'deps'** - Same as stock WP. Default: []; Optional (array).

**'ver'** - Same as stock WP. Default: false; Optional (string).

**'in_footer'** - Same as stock WP. Default: false; Optional (bool).

**'hooks'** - This script is associated with which hooks. Valid values: 'admin', 'front', 'login', 'block', 'block_admin', 'block_front'. Default: []; Optional (array of strings).

**'inline_data'** - See wp_add_inline_script() for more details. Default: ''; Optional (string).

**'inline_position'** - Valid values: 'after', 'before'; A non-false but invalid value will default to after. Default: false; Optional (string).

    use \WPezSuite\WPezClasses\ScriptsRegister\ClassScriptsRegister;
    use \WPezSuite\WPezClasses\ScriptsRegister\ClassHooks;

    $new_sr = new ClassScriptsRegister();
    // push them one by one
    foreach ( $arr_scripts as $arr_script){
        $new_sr->pushScript($arr_script)
    }
    
    // or load them in bulk
    $new_sr->loadScripts($arr_scripts)
    
    // Once you have everything in place, it's hook'em time...
    $new_hooks = new ClassHooks($new_sr);
    $new_hooks->register()
    

### FAQ

**1) Why?**

Because configuring > coding.

Ultimately, arrays are so much easier to work with. If it helps, think of each script as an object with a finite handful of properties. Objects and properties can be updated, etc.


**2) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 




### HELPFUL LINKS

 - https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload
 
 - https://gitlab.com/wpezsuite/WPezClasses/StylesRegister

 - https://developer.wordpress.org/reference/functions/wp_enqueue_script/

 - https://developer.wordpress.org/reference/functions/wp_add_inline_script/

 - https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts

 - https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts

 - https://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts


### TODO

- Provide a better example(s)


### CHANGE LOG

- v0.6.0 - 20 May 2019
    - CHANGED: Now using the WPezClasses Hooks pattern
    - CHANGED: Added support for Gutenberg registering + enqueueing

- v0.5.0 - 11 March 2019
    - Some version of this has been kicking around in my toolbox for quite some time. So it's a new repo and a new version but a not-so-new idea. 
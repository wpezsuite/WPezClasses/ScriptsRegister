<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezClasses\ScriptsRegister;

class ClassScriptsRegister implements InterfaceScriptsRegister{

    protected $_arr_scripts;
    protected $_arr_script_defaults;
    protected $_arr_hooks;
    protected $_arr_registered;


    public function __construct() {

        $this->setPropertyDefaults();

    }


    public function setPropertyDefaults() {

        $this->_arr_scripts = [];

        $this->_arr_script_defaults = [
            'active'          => true,
            'handle'          => false, // required
            'src'             => false,  // required
            'deps'            => [],
            'ver'             => false,
            'in_footer'       => false,
            // where are we using this script/
            'hooks' => [],
            // https://developer.wordpress.org/reference/functions/wp_add_inline_script/
            'inline_data'     => false,
            'inline_position' => false
        ];

	    $this->_arr_hooks = [
		    'admin',
		    'front',
		    'login',
		    'block',
		    'block_admin',
		    'block_front'
	    ];

	    $this->_arr_registered = [];
    }



    public function updateScriptDefaults( $arr_defaults = false ) {

        if ( is_array( $arr_defaults ) ) {

            $this->_arr_script_defaults = array_merge( $this->_arr_script_defaults, $arr_defaults );

            return true;
        }

        return false;
    }


    public function pushScript( $arr_script = false ) {

        if ( is_array( $arr_script ) ) {

            $arr_temp = array_merge( $this->_arr_script_defaults, $arr_script );
            if ( is_string( $arr_temp['handle'] ) && is_array($arr_temp['hooks']) ) {

                $this->_arr_scripts[] = $arr_temp;

                return true;
            }
        }

        return false;
    }


    public function loadScripts( $arr_scripts = false ) {

        if ( is_array( $arr_scripts ) ) {
            $arr_ret = [];
            foreach ( $arr_scripts as $key => $arr_script ) {

                $arr_ret[ $key ] = $this->pushScript( $arr_script );
            }

            return $arr_ret;
        }

        return false;
    }

    public function getScripts() {

        return $this->_arr_scripts;
    }



    public function registerAdmin() {

        $this->registerMaster( 'admin' );
    }

    public function registerFront() {

        $this->registerMaster( 'front' );
    }

    public function registerLogin() {

        $this->registerMaster( 'login' );
    }

	public function registerBlock() {

		$this->registerMaster( 'block' );
	}

	public function registerBlockAdmin() {

		$this->registerMaster( 'block_admin' );
	}

	public function registerBlockFront() {

    	if ( is_admin() ){
    		return;
	    }

		$this->registerMaster( 'block_front' );
	}


    protected function registerMaster( $str_hook = 'front' ) {

        foreach ( $this->_arr_scripts as $arr_script ) {

            if ( $arr_script['active'] === false || in_array($str_hook, $arr_script[ 'hooks' ]) === false ) {
                continue;
            }

            $obj = (object)$arr_script;

            // no need to register a script that's baked into core
            if ( $obj->src === false ){
            	$this->_arr_registered[$str_hook][$obj->handle] = $arr_script;
            	continue;
            }

            $bool_ret = wp_register_script(
                $obj->handle,
                $obj->src,
                $obj->deps,
                $obj->ver,
                $obj->in_footer
            );

            if ( $bool_ret === true ){
	            $this->_arr_registered[$str_hook][$obj->handle] = $arr_script;
            }
        }
    }

    public function enqueueAdmin() {

        $this->enqueueMaster( 'admin' );
    }

    public function enqueueFront() {

        $this->enqueueMaster( 'front' );
    }

    public function enqueueLogin() {

        $this->enqueueMaster( 'login' );
    }

	public function enqueueBlock() {

		$this->enqueueMaster( 'block' );
	}

	public function enqueueBlockAdmin() {

		$this->enqueueMaster( 'block_admin' );
	}

	public function enqueueBlockFront() {

    	if ( is_admin() ) {
    		return;
	    }

		$this->enqueueMaster( 'block_front' );
	}

    protected function enqueueMaster( $str_hook = 'front' ) {

	    if ( ! isset( $this->_arr_registered[ $str_hook ] ) || ! is_array( $this->_arr_registered[ $str_hook ] ) ) {
		    return;
	    }

        foreach ($this->_arr_registered[ $str_hook ] as $str_handle => $arr_script ) {

            $obj = (object)$arr_script;
            wp_enqueue_script( $obj->handle );

            if ( is_string($obj->inline_data) && ! empty($obj->inline_data) && is_string($obj->inline_position) ){
                $str_pos = 'after';
                if ( $obj->inline_position === 'before' ){
                    $str_pos = 'before';
                }

                // TODO - this returns a bool. let's keep passing it back
                wp_add_inline_script($obj->handle, $obj->inline_data, $str_pos);
            }
        }
    }


}